﻿using Microsoft.Win32;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace WPF___DotNet_Traject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        List<Cursist> lijstCursisten = new List<Cursist>();
        ImageSource dezeAfbeelding;
        bool checkBestaat = false;
        string directoryAfbeeldingen = Path.Combine(Environment.CurrentDirectory, "Images");

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Cursist cursist1 = new Cursist("Robin", "Boone", new BitmapImage(new Uri(directoryAfbeeldingen + @"\Robin.PNG", UriKind.Absolute)));
            lijstCursisten.Add(cursist1);
            Cursist cursist2 = new Cursist("Tycho", "Seghers", new BitmapImage(new Uri(directoryAfbeeldingen + @"\Tycho.PNG", UriKind.Absolute)));
            lijstCursisten.Add(cursist2);
            Cursist cursist3 = new Cursist("Jeffrey", "Lenoor", new BitmapImage(new Uri(directoryAfbeeldingen + @"\Jeffrey.PNG", UriKind.Absolute)));
            lijstCursisten.Add(cursist3);

            RefreshListbox();
            RefreshCombobox();
        }

        private void btnAfbeeldingZoeken_Click(object sender, RoutedEventArgs e)
        {

            if (Directory.Exists(directoryAfbeeldingen))
            {
                OpenFileDialog openFileDialog = new OpenFileDialog
                {
                    InitialDirectory = directoryAfbeeldingen,
                    Filter = "Afbeeldingen|*.jpg;*.jpeg;*.png;*.gif|Alle bestanden|*.*"
                };

                if (openFileDialog.ShowDialog() == true)
                {
                    string imagePath = openFileDialog.FileName;
                    dezeAfbeelding = new BitmapImage(new Uri(imagePath, UriKind.Absolute));
                }
            }
            else
            {
                MessageBox.Show("De map met afbeeldingen kon niet gevonden worden.", "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void btnOpslaan_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!((String.IsNullOrEmpty(txtVoornaam.Text)) && (String.IsNullOrEmpty(txtAchternaam.Text))))
                {
                    if (dezeAfbeelding != null)
                    {
                        Cursist geselecteerdeCursist = cmbCursisten.SelectedItem as Cursist;
                        if (geselecteerdeCursist == null)
                        {
                            NieuweCursistToevoegen();
                        }
                        else
                        {
                            foreach (Cursist c in lijstCursisten)
                            {
                                if (c.Equals(geselecteerdeCursist))
                                {
                                    checkBestaat = true;
                                    break;
                                }
                            }
                            if (checkBestaat == true)
                            {
                                GegevensCursistWijzigen(geselecteerdeCursist);
                            }
                            else
                            {
                                NieuweCursistToevoegen();
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Gelieve een afbeelding te selecteren aub.", "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    MessageBox.Show("Gelieve Voornaam en Achternaam in te vullen aub.", "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
            catch
            {
                MessageBox.Show("Er is iets foutgelopen bij het opslaan van de gegevens.", "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }
        private void cmbCursisten_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (cmbCursisten.SelectedItem != null)
            {
                Cursist geselecteerdeCursist = cmbCursisten.SelectedItem as Cursist;
                if (geselecteerdeCursist != null)
                {
                    txtVoornaam.Text = geselecteerdeCursist.Voornaam;
                    txtAchternaam.Text = geselecteerdeCursist.Achternaam;
                    dezeAfbeelding = geselecteerdeCursist.Avatar;
                }
            }
        }
        private void btnVerwijderen_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cmbCursisten.SelectedItem != null)
                {
                    MessageBoxResult result = MessageBox.Show("Ben je zeker dat je deze cursist wit verwijderen?", "Vraag", MessageBoxButton.YesNo, MessageBoxImage.Question);

                    if (result == MessageBoxResult.Yes)
                    {
                        Cursist geselecteerdeCursist = cmbCursisten.SelectedItem as Cursist;

                        if (geselecteerdeCursist != null)
                        {
                            lijstCursisten.Remove(geselecteerdeCursist);

                            RefreshCombobox();
                            RefreshListbox();
                            TXTLeegmaken();

                            MessageBox.Show("De cursist werd verwijderd!", "Verwijderd", MessageBoxButton.OK);
                        }
                    }
                    else
                    {
                        MessageBox.Show("De cursist werd NIET verwijderd!", "Verwijderd", MessageBoxButton.OK);
                    }
                }
                else
                {
                    MessageBox.Show("Selecteer eerst een cursist aub.", "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
            catch
            {
                MessageBox.Show("Er is een fout opgetreden bij het verwijderen van de cursist.", "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }
        private void RefreshListbox()
        {
            lbCursisten.ItemsSource = null;
            lbCursisten.ItemsSource = lijstCursisten;
        }
        private void TXTLeegmaken()
        {
            txtAchternaam.Text = string.Empty;
            txtVoornaam.Text = string.Empty;
        }
        private void RefreshCombobox()
        {
            cmbCursisten.ItemsSource = null;
            cmbCursisten.ItemsSource = lijstCursisten;
        }
        private void NieuweCursistToevoegen()
        {
            try
            {
                foreach (Cursist c in lijstCursisten)
                {
                    if (c.Voornaam.ToUpper().Contains(txtVoornaam.Text.ToUpper()) && c.Achternaam.ToUpper().Contains(txtAchternaam.Text.ToUpper()))
                    {
                        checkBestaat = true;
                        break;
                    }
                }
                if (checkBestaat == false)
                {
                    Cursist dezeCursist = new Cursist(txtVoornaam.Text, txtAchternaam.Text, dezeAfbeelding);
                    lijstCursisten.Add(dezeCursist);

                    RefreshCombobox();
                    RefreshListbox();

                    MessageBox.Show("Welkom " + txtVoornaam.Text + "!", "Welkom", MessageBoxButton.OK);

                    TXTLeegmaken();
                }
                else
                {
                    MessageBox.Show("Deze cursist bestaat reeds.", "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    TXTLeegmaken();
                }
                checkBestaat = false;
            }
            catch
            {
                MessageBox.Show("Er is een fout opgetreden bij het opslaan van de cursist.", "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }
        private void GegevensCursistWijzigen(Cursist geselecteerdeCursist)
        {
            geselecteerdeCursist.Voornaam = txtVoornaam.Text;
            geselecteerdeCursist.Achternaam = txtAchternaam.Text;
            geselecteerdeCursist.Avatar = dezeAfbeelding;

            RefreshCombobox();
            RefreshListbox();
            TXTLeegmaken();

            MessageBox.Show("De gegevens werden aangepast.", "Info", MessageBoxButton.OK);
        }


    }
}