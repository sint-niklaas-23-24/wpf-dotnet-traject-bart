﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace WPF___DotNet_Traject
{
    class Persoon
    {
        private string _achternaam;
        private ImageSource _avatar;
        private string _voornaam;
        private Guid _eenGuid;

        public Persoon() 
        { 
            _eenGuid = Guid.NewGuid();
        }
        public Persoon(string eenVoornaam, string eenAchternaam, ImageSource eenAvatar)
        {
            _eenGuid= Guid.NewGuid();
            Voornaam = eenVoornaam;
            Achternaam = eenAchternaam;
            Avatar = eenAvatar;
        }

        public string Achternaam
        {
            get { return _achternaam; }
            set { _achternaam = value; }
        }
        public string Voornaam
        {
            get { return _voornaam; }
            set { _voornaam = value; }
        }
        public ImageSource Avatar
        {
            get { return _avatar; }
            set { _avatar = value; }
        }
        public Guid EenGuid { get { return _eenGuid; } }

        public override string ToString()
        {
            return Avatar.ToString() + "  " + Voornaam + " " + Achternaam;
        }
        public override bool Equals(object? obj)
        {
            bool resultaat = false;

            if (obj != null)
            {
                if (GetType() == obj.GetType())
                {
                    Persoon r = (Persoon)obj;
                    if (this._eenGuid == r._eenGuid)
                    {
                        resultaat = true;
                    }
                }
            }
            return resultaat;
        }

    }
}
