﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace WPF___DotNet_Traject
{
    class Cursist : Persoon
    {

        public Cursist() : base() 
        {
        }
        public Cursist(string eenVoornaam, string eenAchternaam, ImageSource eenAvatar) : base (eenVoornaam, eenAchternaam, eenAvatar)
        {
        }

        public override string ToString()
        {
            return base.ToString() + "  " + EenGuid;
        }
    }
}
