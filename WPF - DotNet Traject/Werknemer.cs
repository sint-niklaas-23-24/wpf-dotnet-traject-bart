﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace WPF___DotNet_Traject
{
    class Werknemer : Persoon
    {
        private double _loon;

        public Werknemer(): base() 
        {
        }
        public Werknemer(string eenVoornaam, string eenAchternaam, ImageSource eenAvatar, double eenLoon) : base(eenVoornaam, eenAchternaam, eenAvatar) 
        { 
            Loon = eenLoon;
        }

        public double Loon { get { return _loon; } set { _loon = value; } }

        public override string ToString()
        {
            return base.ToString() + "  " + "€ " + Loon;
        }
    }
}
